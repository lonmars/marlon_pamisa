package DBModel	
import (
	"fmt"
	"time"
	_ "github.com/go-sql-driver/mysql"
	"database/sql"
)

func Ap_sql(sq string , sq_type int) (*sql.Rows,error,bool,sql.Result) {
	db, err := sql.Open("mysql", SqlConnString)
	//db, err := sql.Open("mssql", dsn)
	//db.SetConnMaxLifetime(300)
	if err != nil {
		fmt.Println("Cannot connect: ", err.Error())
		defer db.Close()
		return nil, err,false ,nil
	}
	err = db.Ping()
	if err != nil {
		fmt.Println("Cannot connect: ", err.Error())
		defer db.Close()
		return nil, err,false ,nil
	}

	stmt, err_pr := db.Prepare(sq)
    if err_pr != nil {
      fmt.Println(`error db prepare:`,err_pr)
      //log.Fatal(err_pr)
    }


	if sq_type==1{
		rows, err  := stmt.Query()
		fmt.Println("option 1 err: ", err)
		defer db.Close()
		return rows ,err ,false,nil
	}else if sq_type==2{
		rows, err  := stmt.Query()
		//fmt.Println("option 2 err: ", err)
		defer db.Close()
		return rows ,err ,false	,nil	
	}else {
		res , err  := stmt.Exec()
		//fmt.Println("option 3 err: ", err)
		defer db.Close()
		return nil ,err ,true,res
	}	

	fmt.Println(err)
	defer db.Close()
	//fmt.Println("%v", err)
	return nil ,err ,true,nil

}

func PrintValue(pval *interface{}) string{
	var a string
	switch v := (*pval).(type) {
	case nil:
//		fmt.Print("NULL")
		a =fmt.Sprint("NULL")
	case bool:
		if v {
//			fmt.Print("1")
		a =	fmt.Sprint("1")
		} else {
//			fmt.Print("0")
		a =	fmt.Sprint("0")
		}
	case []byte:
//		fmt.Print(string(v))
		a = fmt.Sprint(string(v))
	case time.Time:
		//date formating
		a = fmt.Sprint(v.Format("01/02/2006"))

	default:
//		fmt.Print(v)
		a= fmt.Sprint(v)
	}
	return a
}

func DataList(sql string ) [][]string {
	
	var m  [][]string
	rows ,err, _,_ := Ap_sql(sql,1)
    if err != nil {
    	panic(err.Error())
    }
   	defer rows.Close()
	cols, err := rows.Columns()
	if err != nil {
		fmt.Println(err)
	}
	if cols == nil {
		fmt.Println(err)
	}
 	
	//getting the column name
	vals := make([]interface{}, len(cols))
	for i := 0; i < len(cols); i++ {
		vals[i] = new(interface{})
	}
	//type containerdata [][]interface{}
	for rows.Next() {
		err = rows.Scan(vals...)
		if err != nil {
			fmt.Println(err)
			continue
		}
		//mars at work here
		var a []string
		
		for i := 0; i < len(cols); i++ {  // fetching data tables to container map array
			a  = append(a, PrintValue(vals[i].(*interface{}))  )
		}
		m = append( m,a)
		//mars at work here
	}
		
	if rows.Err() != nil {
		fmt.Println(rows.Err)
	}	
	return m

}

func Data_row(sql string ) []string {
	
	var m  []string
	rows ,err, _,_ := Ap_sql(sql,1)
    if err != nil {
    	panic(err.Error())
    }
   	defer rows.Close()
	cols, err := rows.Columns()
	if err != nil {
		fmt.Println(err)
	}
	if cols == nil {
		fmt.Println(err)
	}
 	
	//getting the column name
	vals := make([]interface{}, len(cols))
	for i := 0; i < len(cols); i++ {
		vals[i] = new(interface{})
	}
	//type containerdata [][]interface{}
	for rows.Next() {
		err = rows.Scan(vals...)
		if err != nil {
			fmt.Println(err)
			continue
		}
		//mars at work here
		var a []string
		
		for i := 0; i < len(cols); i++ {  // fetching data tables to container map array
			a  = append(a, PrintValue(vals[i].(*interface{}))  )
			
		}
		m = a
		//mars at work here
	}
		
	if rows.Err() != nil {
		fmt.Println(rows.Err)
	}	
	return m

}