package Helper

import (
    "bytes"
    "fmt"
    "net/smtp"
    "runtime"
    "strings"
    "text/template"
    "io"
    "crypto/aes"
    "crypto/cipher"
    "crypto/rand"
    "encoding/base64"
)


func Encrypt(key []byte, text string) string {
    // key := []byte(keyText)
    plaintext := []byte(text)
 
    block, err := aes.NewCipher(key)
    if err != nil {
        panic(err)
    }
 
    // The IV needs to be unique, but not secure. Therefore it's common to
    // include it at the beginning of the ciphertext.
    ciphertext := make([]byte, aes.BlockSize+len(plaintext))
    iv := ciphertext[:aes.BlockSize]
    if _, err := io.ReadFull(rand.Reader, iv); err != nil {
        panic(err)
    }
 
    stream := cipher.NewCFBEncrypter(block, iv)
    stream.XORKeyStream(ciphertext[aes.BlockSize:], plaintext)
 
    // convert to base64
    return base64.URLEncoding.EncodeToString(ciphertext)
}
 
// decrypt from base64 to decrypted string
func Decrypt(key []byte, cryptoText string) string {
    ciphertext, _ := base64.URLEncoding.DecodeString(cryptoText)
 
    block, err := aes.NewCipher(key)
    if err != nil {
        panic(err)
    }
 
    // The IV needs to be unique, but not secure. Therefore it's common to
    // include it at the beginning of the ciphertext.
    if len(ciphertext) < aes.BlockSize {
        panic("ciphertext too short")
    }
    iv := ciphertext[:aes.BlockSize]
    ciphertext = ciphertext[aes.BlockSize:]
 
    stream := cipher.NewCFBDecrypter(block, iv)
 
    // XORKeyStream can work in-place if the two arguments are the same.
    stream.XORKeyStream(ciphertext, ciphertext)
 
    return fmt.Sprintf("%s", ciphertext)
}

func catchPanic(err *error, functionName string) {
    if r := recover(); r != nil {
        fmt.Printf("%s : PANIC Defered : %v\n", functionName, r)

        // Capture the stack trace
        buf := make([]byte, 10000)
        runtime.Stack(buf, false)

        fmt.Printf("%s : Stack Trace : %s", functionName, string(buf))

        if err != nil {
            *err = fmt.Errorf("%v", r)
        }
    } else if err != nil && *err != nil {
        fmt.Printf("%s : ERROR : %v\n", functionName, *err)

        // Capture the stack trace
        buf := make([]byte, 10000)
        runtime.Stack(buf, false)

        fmt.Printf("%s : Stack Trace : %s", functionName, string(buf))
    }
}

func SendEmail(host string, port int, userName string, password string, to []string, subject string, message string ,mapdata map[string]string ) (err error) {
    defer catchPanic(&err, "SendEmail")

    parameters := struct {
        From string
        To string
        Subject string
        Message string
    }{
        userName,
        strings.Join([]string(to), ","),
        subject,
        message,
    }

    buffer := new(bytes.Buffer)

    template := template.Must(template.New("emailTemplate").Parse(emailScript(mapdata)))
    template.Execute(buffer, &parameters)

    auth := smtp.PlainAuth("", userName, password, host)

    err = smtp.SendMail(
        fmt.Sprintf("%s:%d", host, port),
        auth,
        userName,
        to,
        buffer.Bytes())

    return err
}

func emailScript(mapdata map[string]string) (string) {
    return `From: {{.From}}
To: {{.To}}
Subject: {{.Subject}}
MIME-version: 1.0
Content-Type: text/html; charset="UTF-8"

{{.Message}}

<html><body>
<table rules="all" style="border-color: #666;" cellpadding="10">
<tr style='background: #eee;'>

    
        
</tr>
</table>
</body></html>


`
}